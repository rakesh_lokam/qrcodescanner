var Campaign = require('../models/campaign.model');
var async = require('async');

exports.campaign_details_getall = function(req, res, next) {
    async.parallel({
        campaign : function(callback){
            Campaign.find({}).exec(callback);
        }
    }, function(err, results){
        if(err) { return next(err)};
        if(results.campaign == null) {
            var err = new Error('product not found');
            err.status = 404;
            return next(err);
        };
        res.json(results);
    })
};
