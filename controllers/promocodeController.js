var PromoCode = require('../models/promocode.model');
var async = require('async');

exports.promocode_details_get = function(req, res, next) {
    async.parallel({
        promocode: function(callback){
            PromoCode.find({})
            .populate('campaignId')
            .populate('productId')
            .exec(callback);
        }

    }, function(err, results){
        if(err) { return next(err)};
        if(results.promocode == null) {
            var err = new Error('product not found');
            err.status = 404;
            return next(err);
        }
        res.json(results);
    });
};

exports.promocode_detail = function(req, res, next) {
    async.parallel({
        promocode: function(callback){
            PromoCode.find({"code" : req.params.id})
            .populate('campaignId')
            .populate('productId')
            .exec(callback);
        }

    }, function(err, results){
        if(err) { return next(err)};
        if(results.promocode == null) {
            var err = new Error('product not found');
            err.status = 404;
            return next(err);
        }
        res.json(results);
    });
};

exports.promocode_used = function (req, res, next) {
    async.parallel({ 
        promocode: function(callback){
            PromoCode.update({
                "code" : req.params.id
            }, 
            {
                $set: { "isUsed": true}
            }, 
            {
                upsert : false,
                strict: false
            }).exec(callback);
        }
    }, function(err, results){
        if(err) { return next(err);}
        if(results.promocode == null) {
            var err = new Error('product not found');
            err.status = 404;
            return next(err);
        }
        res.json(results);
    });
};