var Product = require('../models/product.model');
var async = require('async');

exports.product_details = function(req, res, next) {
    async.parallel({
        product: function(callback){
            Product.findById(req.params.id).exec(callback);
        }
    }, function(err, results){
        if (err) { return next(err); }
        if (results.product ==null) { // No results.
            var err = new Error('product not found');
            err.status = 404;
            return next(err);
    }
        console.log(json.stringify(results));
        res.json(results);
    }
    );
  
};

exports.product_details_getall = function(req, res) {
    async.parallel({
        product : function(callback) {
            Product.find({}).exec(callback);
        }
    }, function (err, results){
        if (err) { return next(err); }
        if (results.product ==null) { // No results.
            var err = new Error('product not found');
            err.status = 404;
            return next(err);
        }
         res.json(results);
    });
        // Product.find({}).exec(function (err, product) {
        //   if (err) { return next(err); }
        //   //Successful, so render
        //   //res.render('book_list', { title: 'Book List', book_list: list_books });
        //  res.json(product);
        // });
};