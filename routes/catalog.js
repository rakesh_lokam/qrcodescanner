var express = require('express');
var router = express.Router();

var product_controller = require('../controllers/productController');
var campaign_controller = require('../controllers/campaignController');
var promocode_controller = require('../controllers/promocodeController');

router.get('/products', product_controller.product_details_getall);
router.get('/campaigns', campaign_controller.campaign_details_getall);
router.get('/promocode', promocode_controller.promocode_details_get);
router.get('/promocode/:id', promocode_controller.promocode_detail);
router.put('/promocode/:id', promocode_controller.promocode_used);

module.exports = router;