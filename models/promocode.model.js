var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var promocodeSchema = new Schema({
    code: {type: String, required: true},
    campaignId: {type: Schema.ObjectId, ref: 'Campaign', required: true},
    productId: {type: Schema.ObjectId, ref: 'Product', required: true },
    used: {type:Boolean}
});


module.exports = mongoose.model('Promocode', promocodeSchema);