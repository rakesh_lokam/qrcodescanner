var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var campaignSchema = new Schema({
    id: {type: String, required: true},
    name: {type: String, required: true},
    type: {type: String, required: true},
    local: {type: String, required: true},
    isActive: {type: Boolean, required: true}
});

module.exports = mongoose.model('Campaign', campaignSchema);