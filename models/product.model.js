var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
    name: {type: String, required: true},
    category: {type: String, required: true},
    gtin: {type: Number, required: true},
    image: {type: String, required: true}
});

productSchema
.virtual('url')
.get(function () {
    return '/catalog/products' + this._id;
  });
  
module.exports= mongoose.model('Product', productSchema);